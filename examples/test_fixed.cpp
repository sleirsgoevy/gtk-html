#include "gtk-html/gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            <fixed>
                <span x=0 y=0>top left</span>
                <span x=100 y=0>top right</span>
                <span x=0 y=100>bottom left</span>
                <span x=100 y=100>bottom right</span>
            </fixed>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
