#include "gtk-html/gtk-html.hpp"
#include <iostream>

struct MyApp
{
    GTKHTML_NAMED_OBJECTS();

    GtkWidget* test_button;
    GTKHTML_BY_ID(test_button);

    GTKHTML_CALLBACKS();

    void clicked_456(GtkButton* w)
    {
        std::cout << "456 clicked!" << std::endl;
        gtk_widget_set_visible(test_button, FALSE);
    }
    GTKHTML_CALLBACK(clicked_456);

    void clicked_test(GtkButton* w)
    {
        std::cout << "test clicked!" << std::endl;
    }
    GTKHTML_CALLBACK(clicked_test);

    static void activate(GtkApplication* app)
    {
        GTKHTML(R"(
    <window title="window title" show noresize>
        <div>123<button onclick=clicked_456>456</button>789</div>
        <button id=test_button onclick=clicked_test>test</button>
        <div>test text</div>
    </window>
    )", app, new MyApp());
    }
};

static void run_app(GtkApplication* app)
{
    MyApp::activate(app);
}
GTKHTML_GLOBAL_CALLBACK(run_app);

int main()
{
    g_object_unref(GTKHTML("<application id=org.gtk.html onactivate=run_app run/>"));
}
