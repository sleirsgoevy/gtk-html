#include "gtk-html/gtk-html.hpp"
#include <iostream>

using namespace std;

GtkWidget* q;
GTKHTML_GLOBAL_BY_ID(q);

static void run(GtkApplication* app)
{
    GtkWidget* g0 = GTKHTML("<button/>");
    GtkWidget* g1 = GTKHTML("<widget id=q/>", g0);
    if(g0 != g1 || g1 != q)
        std::cout << "fail" << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
