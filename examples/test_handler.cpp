#include "gtk-html/gtk-html.hpp"
#include <iostream>

static GtkWidget* handler_self;
GTKHTML_GLOBAL_BY_ID(handler_self);
static gulong handler_id;
GTKHTML_GLOBAL_BY_ID(handler_id);
static bool have_unset = false;

static void onclick(GtkButton* btn)
{
    if(!have_unset)
    {
        have_unset = 1;
        g_signal_handler_disconnect(handler_self, handler_id);
    }
}
GTKHTML_GLOBAL_CALLBACK(onclick);

static void onclick2(GtkButton* btn)
{
    std::cout << "hi" << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(onclick2);

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            <button onclick=onclick>Unset</button>
            <button onclick=<handler id=handler_id base=handler_self>onclick2</handler>>Test</button>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
