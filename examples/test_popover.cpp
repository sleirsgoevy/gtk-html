#include "gtk-html/gtk-html.hpp"

static GtkWidget* po;
GTKHTML_GLOBAL_BY_ID(po);

static void click1(GtkButton* btn)
{
    gtk_popover_popup(GTK_POPOVER(po));
}
GTKHTML_GLOBAL_CALLBACK(click1);

static void click2(GtkButton* btn)
{
    gtk_popover_popdown(GTK_POPOVER(po));
}
GTKHTML_GLOBAL_CALLBACK(click2);

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            <button onclick=click1 hexpand>Show</button>
            <button onclick=click2 hexpand>Hide</button>
            <popover id=po vexpand/>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
