#include "gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window show>
            <stack id=stk>
                <button id=retry onclick=switch_to_target>Click me for free cookies!</button>
                <div id=target>
                    Sorry, we're out of stock :-(
                    <div><button onclick=try_again>Try again</button></div>
                </div>
            </stack>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

GtkWidget* stk;
GTKHTML_GLOBAL_BY_ID(stk);
GtkWidget* retry;
GTKHTML_GLOBAL_BY_ID(retry);
GtkWidget* target;
GTKHTML_GLOBAL_BY_ID(target);

static void switch_to_target(GtkButton* btn)
{
    gtk_stack_set_visible_child(GTK_STACK(stk), target);
}
GTKHTML_GLOBAL_CALLBACK(switch_to_target);

static void try_again(GtkButton* btn)
{
    gtk_stack_set_visible_child(GTK_STACK(stk), retry);
}
GTKHTML_GLOBAL_CALLBACK(try_again);

int main()
{
    GTKHTML("<application id=org.gtk.html onactivate=run run/>");
}
