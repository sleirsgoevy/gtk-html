#include "gtk-html/gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            <table>
                <tr><td>1</td><td rowspan=2>2&5</td><td>3</td></tr>
                <tr><td>4</td><td>6</td></tr>
                <tr><td colspan=2>7&8</td><td>9</td></tr>
            </table>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
