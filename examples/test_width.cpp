#include "gtk-html/gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML("<window noresize show><button width=100/></window>", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
    return 0;
}
