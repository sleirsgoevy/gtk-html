#include "gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window show>
            <notebook id=main_nb>
                <div>123</div>
                <div>456</div>
                <notebook-tab id=pagenr label=789>789</notebook-tab>
                <button onclick=set_page>Set page 789</button>
            </notebook>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

GtkWidget* main_nb;
GTKHTML_GLOBAL_BY_ID(main_nb);

int pagenr;
GTKHTML_GLOBAL_BY_ID(pagenr);

static void set_page(GtkButton* btn)
{
    gtk_notebook_set_current_page(GTK_NOTEBOOK(main_nb), pagenr);
}
GTKHTML_GLOBAL_CALLBACK(set_page);

int main()
{
    GTKHTML("<application id=org.gtk.html onactivate=run run/>");
}
