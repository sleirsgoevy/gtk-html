#include "gtk-html/gtk-html.hpp"
#include <iostream>

GtkWidget* spb0;
GTKHTML_GLOBAL_BY_ID(spb0);

static void onchange(GtkSpinButton* spb)
{
    if(GTK_SPIN_BUTTON(spb0) != spb)
        std::cout << "warning: spb0 != spb" << std::endl;
    std::cout << gtk_spin_button_get_value(spb) << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(onchange);

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            Enter an even number between 1 and 20: <input id=spb0 type=number min=2 max=20 step=2 value=10 onchange=onchange/>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
