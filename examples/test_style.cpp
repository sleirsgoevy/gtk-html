#include "gtk-html/gtk-html.hpp"

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show>
            <button style="background: black; color: red">outer<button>inner</button></button>
            <button>outer1</button>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
