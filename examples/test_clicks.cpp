#include "gtk-html/gtk-html.hpp"
#include <iostream>

static void test1(GtkGestureClick*, int, double, double)
{
    std::cout << "test1" << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(test1);

static void test2(GtkGestureClick*, int, double, double)
{
    std::cout << "test2" << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(test2);

static void run(GtkApplication* app)
{
    GTKHTML("<window noresize show><span onclick=test1 oncontextmenu=test2>123</span></window>", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
