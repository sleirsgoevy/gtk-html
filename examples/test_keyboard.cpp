#include "gtk-html/gtk-html.hpp"
#include <iostream>

static void onkeydown(void* q, unsigned a, unsigned b, GdkModifierType c)
{
    std::cout << "keydown " << a << ' ' << b << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(onkeydown);

static void onkeyup(void* q, unsigned a, unsigned b, GdkModifierType c)
{
    std::cout << "keyup " << a << ' ' << b << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(onkeyup);

static void run(GtkApplication* app)
{
    GTKHTML(R"(
        <window noresize show onkeydown=onkeydown onkeyup=onkeyup>
            <input/>
            <button>test</button>
        </window>
    )", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.test onactivate=run run/>");
}
