#include "gtk-html/gtk-html.hpp"
#include <iostream>

void onchange(GtkRange* rng)
{
    std::cout << gtk_range_get_value(rng) << std::endl;
}
GTKHTML_GLOBAL_CALLBACK(onchange);

void run(GtkApplication* app)
{
    GTKHTML("<window noresize show><input type=range min=0 max=100 width=500 onchange=onchange/></window>", app);
}
GTKHTML_GLOBAL_CALLBACK(run);

int main()
{
    GTKHTML("<application id=tk.sleirsgoevy.slider onactivate=run run/>");
    return 0;
}
