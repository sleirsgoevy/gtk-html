#ifndef _CALL_TREE_H
#define _CALL_TREE_H
#include "parser.hpp"

namespace call_trees
{

namespace imports
{

template<typename N>
struct FunctionAccessor
{
    static_assert(sizeof(N) != sizeof(N), "unknown function requested");
    template<typename... Args>
    static int call(Args... args)
    {
        return 0;
    }
};

template<typename N>
struct ConstantAccessor
{
    static_assert(sizeof(N) != sizeof(N), "unknown constant requested");
    static constexpr int value = 0;
};

template<typename N, int sz>
struct TypeName{};

template<typename N>
struct TypeAccessor
{
    static_assert(sizeof(N) != sizeof(N), "unknown type requested");
    using value = int;
};

}

enum class call_tree_type
{
    FUNCTION_CALL,
    INT_CONST,
    STR_CONST,
    DOUBLE_CONST,
    NAMED_CONST,
    ARG_UNSHIFT,
    ARG_SHIFT,
    ARG_IDX,
    COMMA,
    CALLBACK,
    CALLBACK_UAP,
    PUT_ID,
};

struct call_tree;

struct call_function
{
    const char* name;
    const char* caster_name;
    call_tree** args;
};

struct callback
{
    const char* name;
    const char** args;
};

struct id_put
{
    const char* id;
    call_tree* value;
    int nshift;
};

struct comma
{
    call_tree* a;
    call_tree* b;
};

struct call_tree
{
    call_tree_type type;
    /*union
    {*/
        call_function fn;
        callback cb;
        id_put byid;
        int value_i;
        const char* value_s;
        double value_d;
        int* args;
        comma cm;
    //};
};

template<typename P>
struct DoCallTree : P
{
    const call_tree* call_root = nullptr;
    constexpr DoCallTree(const char* p)
    {
        call_root = P::parse_and_implement(p);
    }
};

namespace internal
{

template<typename P, typename S>
struct CallTree
{
    static constexpr S str = S();
    static constexpr P data = P(str.data);
};

template<bool q, typename P0, typename S, int memsz>
struct ReallocatorImpl
{
    static constexpr auto data = CallTree<typename P0::template Impl<memsz>, S>::data;
};

template<typename P0, typename S, int memsz = 32>
struct Reallocator
{
    using ct = CallTree<typename P0::template Impl<memsz>, S>;
    static constexpr auto data = ReallocatorImpl<ct::data.memory_error, P0, S, memsz>::data;
};

template<typename P0, typename S, int memsz>
struct ReallocatorImpl<true, P0, S, memsz> : Reallocator<P0, S, memsz*2>{};

struct DummyCallTreeNode
{
    template<typename... Args>
    static int get(Args... a)
    {
        return 0;
    }
};

template<int cnt>
struct NthArgument
{
    template<typename F, typename... Args>
    static inline auto get(F first, Args... args)
    {
        return NthArgument<cnt-1>::get(args...);
    }
};

template<>
struct NthArgument<0>
{
    template<typename F, typename... Args>
    static inline auto get(F first, Args... args)
    {
        return first;
    }
};

template<typename F>
static inline auto last_argument(F arg)
{
    return arg;
}

template<typename F, typename G, typename... Args>
static inline auto last_argument(F first, G second, Args... args)
{
    return last_argument(second, args...);
}

template<int nskip>
struct LastArgumentWithSkip
{
    template<typename F, typename G, typename... Args>
    static inline auto get(F first, G second, Args... args)
    {
        return LastArgumentWithSkip<nskip-1>::get(first, args...);
    }
    template<typename F>
    static inline auto get(F first)
    {
        return first;
    }
};

template<>
struct LastArgumentWithSkip<0>
{
    template<typename... Args>
    static inline auto get(Args... args)
    {
        return last_argument(args...);
    }
};

struct EmptyString
{
    static constexpr int len = 0;
    constexpr static void write(char* p){}
};

template<char c>
struct CharString
{
    static constexpr int len = 1;
    constexpr static void write(char* p)
    {
        *p = c;
    }
};

template<typename L, typename R>
struct TreeString
{
    static constexpr int len = L::len + R::len;
    constexpr static void write(char* p)
    {
        L::write(p);
        R::write(p+L::len);
    }
};

template<typename S>
struct StaticString
{
    struct Inner
    {
        char data[S::len+1] = {0};
        constexpr Inner()
        {
            S::write(data);
        }
    };
    static constexpr Inner value = Inner();
    static const char* get()
    {
        static const Inner local = value;
        return local.data;
    }
};

template<int l, int s, typename R>
struct GetSubstring
{
    using value = TreeString<typename GetSubstring<l/2, s, R>::value, typename GetSubstring<(l+1)/2, s+l/2, R>::value>;
};

template<int s, typename R>
struct GetSubstring<1, s, R>
{
    using value = CharString<*R::data.self->template internalize<char>(s)>;
};

template<int s, typename R>
struct GetSubstring<0, s, R>
{
    using value = EmptyString;
};

template<int s, typename R>
struct GetString
{
    using value = typename GetSubstring<R::data.self->strlen(R::data.self->template internalize<char>(s)), s, R>::value;
};

template<int l, int s, typename W>
struct GetSubstringFromWrapper
{
    using value = TreeString<typename GetSubstringFromWrapper<l/2, s, W>::value, typename GetSubstringFromWrapper<(l+1)/2, s+l/2, W>::value>;
};

template<int s, typename W>
struct GetSubstringFromWrapper<1, s, W>
{
    using value = CharString<W().data[s]>;
};

template<int s, typename W>
struct GetSubstringFromWrapper<0, s, W>
{
    using value = EmptyString;
};

template<typename W>
static W drop_ref(W x)
{
    return x;
}

struct DefaultScope{};

template<typename C, typename N>
struct IDStoreDelegate
{
    using dC = decltype(drop_ref(**(C*)0));
    using NamedObjectRef = typename dC::template NamedObjectRef<N, false>;
    template<typename V>
    static inline void set(C ctxt, V data)
    {
        NamedObjectRef::set(ctxt, data);
    }
};

template<typename N>
struct IDStoreDelegate<DefaultScope, N>
{
    template<typename V>
    static inline void set(DefaultScope ctxt, V data){}
};

template<typename N, typename C, typename V>
static inline void put_by_id(C ctxt, V value)
{
    IDStoreDelegate<C, N>::set(ctxt, value);
}

template<typename C, typename N>
struct CallbackDelegate
{
    using dC = decltype(drop_ref(**(C*)0));
    using CallbackRef = typename dC::template CallbackRef<N, false>;
    template<typename... Args>
    static inline auto call(void* ctxt, Args... args)
    {
        return CallbackRef::call((C)ctxt, args...);
    }
};

template<typename N>
struct CallbackDelegate<DefaultScope, N>
{
    static_assert(sizeof(N) != sizeof(N), "undefined global callback");
    template<typename... Args>
    static inline int call(DefaultScope ctxt, Args... args)
    {
        return 0;
    }
};

template<typename C>
static inline void* get_callback_uap(C ctxt)
{
    return ctxt;
}

template<>
inline void* get_callback_uap<DefaultScope>(DefaultScope ctxt)
{
    return nullptr;
}

template<int q, typename R>
struct CallTreeImpl;

template<call_tree_type which, typename R, int p>
struct CallTreeNodeImpl : DummyCallTreeNode
{
    static_assert(which != which, "unknown call_tree node type");
};

template<typename R, int p>
struct CallTreeNode
{
    static constexpr const call_tree* node = R::data.self->template internalize<call_tree>(p);
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::PUT_ID, R, p>
{
    static constexpr const call_tree* node = CallTreeNode<R, p>::node;
    template<typename... Args>
    static inline auto get(Args... args)
    {
        auto ans = CallTreeImpl<R::data.self->externalize(node->byid.value), R>::get(args...);
        put_by_id<typename GetString<R::data.self->externalize(node->byid.id), R>::value>(LastArgumentWithSkip<node->byid.nshift>::get(DefaultScope(), args...), ans);
        return ans;
    }
};

template<int l, int s, typename F, typename R, typename... Args0>
struct FunctionCallerImpl
{
    static constexpr call_tree* const* p = R::data.self->template internalize<call_tree*>(s);
    template<typename... Args>
    static inline auto call(Args0... args0, Args... args)
    {
        return FunctionCallerImpl<l-1, R::data.self->externalize(p+1), F, R, Args0...>::call(args0..., args..., CallTreeImpl<R::data.self->externalize(*p), R>::get(args0...));
    }
};

template<int s, typename F, typename R, typename... Args0>
struct FunctionCallerImpl<0, s, F, R, Args0...>
{
    template<typename... Args>
    static inline auto call(Args0... args0, Args... args)
    {
        return F::call(args...);
    }
};

template<typename F, int p, typename R>
struct FunctionCaller
{
    static constexpr const call_tree* q = CallTreeNode<R, p>::node;
    template<typename... Args>
    static inline auto get(Args... args)
    {
        return FunctionCallerImpl<R::data.self->elemlen(q->fn.args), R::data.self->externalize(q->fn.args), F, R, Args...>::call(args...);
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::FUNCTION_CALL, R, p>
    : FunctionCaller<imports::FunctionAccessor<typename GetString<R::data.self->externalize(CallTreeNode<R, p>::node->fn.name), R>::value>, p, R>{};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::STR_CONST, R, p>
{
    template<typename... Args>
    static inline const char* get(Args... args)
    {
        return StaticString<typename GetString<R::data.self->externalize(CallTreeNode<R, p>::node->value_s), R>::value>::get();
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::INT_CONST, R, p>
{
    static constexpr int value = CallTreeNode<R, p>::node->value_i;
    template<typename... Args>
    static constexpr int get(Args... args)
    {
        return value;
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::NAMED_CONST, R, p>
{
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        return imports::ConstantAccessor<typename GetString<R::data.self->externalize(CallTreeNode<R, p>::node->value_s), R>::value>::value;
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::DOUBLE_CONST, R, p>
{
    static constexpr double value = CallTreeNode<R, p>::node->value_d;
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        return value;
    }
};

template<typename T, typename R, typename... Args>
struct CallbackProxy
{
    static inline R call(Args... args, gpointer uap)
    {
        return T::call(uap, args...);
    }
};

template<int l, int s, typename R, typename... Args>
struct CallbackSignatureImpl
{
    static constexpr const char* const* p = R::data.self->template internalize<const char*>(s);
    using type = typename imports::TypeAccessor<typename GetString<R::data.self->externalize(*p), R>::value>::value;
    using value = typename CallbackSignatureImpl<l-1, R::data.self->externalize(p+1), R, Args..., type>::value;
};

template<int s, typename R, typename... Args>
struct CallbackSignatureImpl<0, s, R, Args...>
{
    struct value
    {
        template<typename N, typename C>
        static inline auto get(C ctxt)
        {
            return CallbackProxy<CallbackDelegate<C, N>, Args...>::call;
        }
    };
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::CALLBACK, R, p>
{
    static constexpr const call_tree* node = CallTreeNode<R, p>::node;
    using name = typename GetString<R::data.self->externalize(node->cb.name), R>::value;
    using args = typename CallbackSignatureImpl<R::data.self->elemlen(node->cb.args), R::data.self->externalize(node->cb.args), R>::value;
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        return args::template get<name>(last_argument(DefaultScope(), args...));
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::CALLBACK_UAP, R, p>
{
    template<typename... Args>
    static inline auto get(Args... args)
    {
        return get_callback_uap(last_argument(DefaultScope(), args...));
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::ARG_UNSHIFT, R, p>
{
    static constexpr const call_tree* node = CallTreeNode<R, p>::node;
    static constexpr int left = R::data.self->externalize(node->cm.a);
    static constexpr int right = R::data.self->externalize(node->cm.b);
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        return CallTreeImpl<right, R>::get(CallTreeImpl<left, R>::get(args...), args...);
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::ARG_SHIFT, R, p>
{
    static constexpr const call_tree* node = CallTreeNode<R, p>::node;
    template<typename F, typename... Args>
    static constexpr auto get(F first, Args... args)
    {
        return CallTreeImpl<R::data.self->externalize(node->cm.a), R>::get(args...);
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::ARG_IDX, R, p>
{
    static constexpr int n = CallTreeNode<R, p>::node->value_i;
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        return NthArgument<n>::get(args...);
    }
};

template<typename R, int p>
struct CallTreeNodeImpl<call_tree_type::COMMA, R, p>
{
    static constexpr const call_tree* node = CallTreeNode<R, p>::node;
    static constexpr int left = R::data.self->externalize(node->cm.a);
    static constexpr int right = R::data.self->externalize(node->cm.b);
    template<typename... Args>
    static constexpr auto get(Args... args)
    {
        CallTreeImpl<left, R>::get(args...);
        return CallTreeImpl<right, R>::get(args...);
    }
};

template<int q, typename R>
struct CallTreeImpl
    : CallTreeNodeImpl<R::data.self->template internalize<call_tree>(q)->type, R, q>{};

template<typename R>
struct CallTreeImpl<-1, R>
    : DummyCallTreeNode
{
    static_assert(sizeof(R) != sizeof(R), "null pointer passed to CallTreeImpl");
};

}

using internal::CallTreeImpl;
using internal::Reallocator;

}

#define CALLTREE_FUNCTION(name)\
struct function_name__ ## name\
{\
    char data[sizeof(#name)] = #name;\
};\
template<>\
struct FunctionAccessor<typename ::call_trees::internal::GetSubstringFromWrapper<sizeof(#name)-1, 0, function_name__ ## name>::value>\
{\
    template<typename... Args>\
    static inline auto call(Args... args)\
    {\
        return name(args...);\
    }\
}

#define CALLTREE_CONSTANT(name)\
struct constant_name__ ## name\
{\
    char data[sizeof(#name)] = #name;\
};\
template<>\
struct ConstantAccessor<typename ::call_trees::internal::GetSubstringFromWrapper<sizeof(#name)-1, 0, constant_name__ ## name>::value>\
{\
    static constexpr auto value = name;\
}

#define CALLTREE_TYPE(name)\
template<>\
struct TypeName<name, __COUNTER__>\
{\
    char data[sizeof(#name)] = #name;\
};\
template<>\
struct TypeAccessor<typename ::call_trees::internal::GetSubstringFromWrapper<sizeof(#name)-1, 0, TypeName<name, __COUNTER__-1>>::value>\
{\
    using value = name;\
}

#define GTKHTML_GLOBAL_CALLBACK(name)\
namespace call_trees\
{\
namespace internal\
{\
struct callback_name__ ## name\
{\
    char data[sizeof(#name)] = #name;\
};\
template<>\
struct CallbackDelegate<DefaultScope, typename GetSubstringFromWrapper<sizeof(#name)-1, 0, callback_name__ ## name>::value>\
{\
    template<typename... Args>\
    static inline auto call(void* ctxt, Args... args)\
    {\
        return ::name(args...);\
    }\
};\
}\
}

#define GTKHTML_GLOBAL_BY_ID(name)\
namespace call_trees\
{\
namespace internal\
{\
struct object_name__ ## name\
{\
    char data[sizeof(#name)] = #name;\
};\
template<>\
struct IDStoreDelegate<DefaultScope, typename GetSubstringFromWrapper<sizeof(#name)-1, 0, object_name__ ## name>::value>\
{\
    template<typename V>\
    static inline void set(DefaultScope ctxt, V val)\
    {\
        name = val;\
    }\
};\
}\
}

#define GTKHTML_CALLBACKS()\
template<typename N, bool q>\
struct CallbackRef\
{\
    static_assert(q != q, "undefined local callback");\
    template<typename C, typename... Args>\
    static inline int call(C ctxt, Args... args)\
    {\
        return 0;\
    }\
}

#define GTKHTML_NAMED_OBJECTS()\
template<typename N, bool q>\
struct NamedObjectRef\
{\
    template<typename C, typename V>\
    static inline void set(C ctxt, V value){}\
}

#define GTKHTML_CALLBACK(name)\
struct callback_name__ ## name\
{\
    static constexpr char data[sizeof(#name)] = #name;\
};\
template<bool q>\
struct CallbackRef<typename ::call_trees::internal::GetSubstringFromWrapper<sizeof(#name)-1, 0, callback_name__ ## name>::value, q>\
{\
    template<typename C, typename... Args>\
    static inline auto call(C ctxt, Args... args)\
    {\
        return ctxt->name(args...);\
    }\
}

#define GTKHTML_BY_ID(name)\
struct object_name__ ## name\
{\
    static constexpr char data[sizeof(#name)] = #name;\
};\
template<bool q>\
struct NamedObjectRef<typename ::call_trees::internal::GetSubstringFromWrapper<sizeof(#name)-1, 0, object_name__ ## name>::value, q>\
{\
    template<typename C, typename V>\
    static inline void set(C ctxt, V value)\
    {\
        ctxt->name = value;\
    }\
}

#endif
