#ifndef _GTK_HTML_CPP
#define _GTK_HTML_CPP

#include <gtk/gtk.h>
#include "parser.hpp"
#include "call-tree.hpp"

namespace call_trees
{

namespace imports
{

#define WRAP_MACRO(name)\
template<typename F>\
static inline auto wrap__ ## name(F arg)\
{\
    return name(arg);\
}

WRAP_MACRO(G_APPLICATION);
WRAP_MACRO(G_CALLBACK);
WRAP_MACRO(GTK_BOX);
WRAP_MACRO(GTK_BUTTON);
WRAP_MACRO(GTK_ENTRY);
WRAP_MACRO(GTK_EVENT_CONTROLLER);
WRAP_MACRO(GTK_FIXED);
WRAP_MACRO(GTK_GESTURE_SINGLE);
WRAP_MACRO(GTK_GRID);
WRAP_MACRO(GTK_NOTEBOOK);
WRAP_MACRO(GTK_OVERLAY);
WRAP_MACRO(GTK_PICTURE);
WRAP_MACRO(GTK_POPOVER);
WRAP_MACRO(GTK_RANGE);
WRAP_MACRO(GTK_SCROLLED_WINDOW);
WRAP_MACRO(GTK_SPIN_BUTTON);
WRAP_MACRO(GTK_STACK);
WRAP_MACRO(GTK_STYLE_PROVIDER);
WRAP_MACRO(GTK_TEXT_VIEW);
WRAP_MACRO(GTK_WINDOW);

#undef WRAP_MACRO

template<typename A, typename B, typename C, typename D>
static inline auto wrap__g_signal_connect(A a, B b, C c, D d)
{
    return g_signal_connect(a, b, c, d);
}

CALLTREE_FUNCTION(g_application_run);
CALLTREE_FUNCTION(gtk_application_new);
CALLTREE_FUNCTION(gtk_application_window_new);
CALLTREE_FUNCTION(gtk_box_append);
CALLTREE_FUNCTION(gtk_box_new);
CALLTREE_FUNCTION(gtk_button_new);
CALLTREE_FUNCTION(gtk_button_set_child);
CALLTREE_FUNCTION(gtk_css_provider_load_from_data);
CALLTREE_FUNCTION(gtk_css_provider_new);
CALLTREE_FUNCTION(gtk_entry_buffer_set_text);
CALLTREE_FUNCTION(gtk_entry_get_buffer);
CALLTREE_FUNCTION(gtk_entry_new);
CALLTREE_FUNCTION(gtk_entry_set_visibility);
CALLTREE_FUNCTION(gtk_event_controller_key_new);
CALLTREE_FUNCTION(gtk_fixed_new);
CALLTREE_FUNCTION(gtk_fixed_put);
CALLTREE_FUNCTION(gtk_gesture_click_new);
CALLTREE_FUNCTION(gtk_gesture_drag_new);
CALLTREE_FUNCTION(gtk_gesture_single_set_button);
CALLTREE_FUNCTION(gtk_grid_new);
CALLTREE_FUNCTION(gtk_grid_attach);
CALLTREE_FUNCTION(gtk_label_new);
CALLTREE_FUNCTION(gtk_notebook_append_page);
CALLTREE_FUNCTION(gtk_notebook_new);
CALLTREE_FUNCTION(gtk_notebook_set_action_widget);
CALLTREE_FUNCTION(gtk_overlay_add_overlay);
CALLTREE_FUNCTION(gtk_overlay_new);
CALLTREE_FUNCTION(gtk_overlay_set_child);
CALLTREE_FUNCTION(gtk_picture_new);
CALLTREE_FUNCTION(gtk_picture_new_for_filename);
CALLTREE_FUNCTION(gtk_picture_set_can_shrink);
CALLTREE_FUNCTION(gtk_popover_new);
CALLTREE_FUNCTION(gtk_popover_set_child);
CALLTREE_FUNCTION(gtk_range_set_value);
CALLTREE_FUNCTION(gtk_scale_new_with_range);
CALLTREE_FUNCTION(gtk_scrolled_window_new);
CALLTREE_FUNCTION(gtk_scrolled_window_set_child);
CALLTREE_FUNCTION(gtk_spin_button_new_with_range);
CALLTREE_FUNCTION(gtk_spin_button_set_value);
CALLTREE_FUNCTION(gtk_stack_add_child);
CALLTREE_FUNCTION(gtk_stack_new);
CALLTREE_FUNCTION(gtk_style_context_add_provider);
CALLTREE_FUNCTION(gtk_text_buffer_set_text);
CALLTREE_FUNCTION(gtk_text_view_get_buffer);
CALLTREE_FUNCTION(gtk_text_view_new);
CALLTREE_FUNCTION(gtk_text_view_set_cursor_visible);
CALLTREE_FUNCTION(gtk_text_view_set_editable);
CALLTREE_FUNCTION(gtk_text_view_set_wrap_mode);
CALLTREE_FUNCTION(gtk_widget_add_controller);
CALLTREE_FUNCTION(gtk_widget_get_style_context);
CALLTREE_FUNCTION(gtk_widget_set_halign);
CALLTREE_FUNCTION(gtk_widget_set_hexpand);
CALLTREE_FUNCTION(gtk_widget_set_size_request);
CALLTREE_FUNCTION(gtk_widget_set_valign);
CALLTREE_FUNCTION(gtk_widget_set_vexpand);
CALLTREE_FUNCTION(gtk_widget_show);
CALLTREE_FUNCTION(gtk_window_fullscreen);
CALLTREE_FUNCTION(gtk_window_set_child);
CALLTREE_FUNCTION(gtk_window_set_modal);
CALLTREE_FUNCTION(gtk_window_set_resizable);
CALLTREE_FUNCTION(gtk_window_set_title);
CALLTREE_FUNCTION(wrap__G_APPLICATION);
CALLTREE_FUNCTION(wrap__G_CALLBACK);
CALLTREE_FUNCTION(wrap__GTK_BOX);
CALLTREE_FUNCTION(wrap__GTK_BUTTON);
CALLTREE_FUNCTION(wrap__GTK_ENTRY);
CALLTREE_FUNCTION(wrap__GTK_EVENT_CONTROLLER);
CALLTREE_FUNCTION(wrap__GTK_FIXED);
CALLTREE_FUNCTION(wrap__GTK_GESTURE_SINGLE);
CALLTREE_FUNCTION(wrap__GTK_GRID);
CALLTREE_FUNCTION(wrap__GTK_NOTEBOOK);
CALLTREE_FUNCTION(wrap__GTK_OVERLAY);
CALLTREE_FUNCTION(wrap__GTK_PICTURE);
CALLTREE_FUNCTION(wrap__GTK_POPOVER);
CALLTREE_FUNCTION(wrap__GTK_RANGE);
CALLTREE_FUNCTION(wrap__GTK_SCROLLED_WINDOW);
CALLTREE_FUNCTION(wrap__GTK_SPIN_BUTTON);
CALLTREE_FUNCTION(wrap__GTK_STACK);
CALLTREE_FUNCTION(wrap__GTK_STYLE_PROVIDER);
CALLTREE_FUNCTION(wrap__GTK_TEXT_VIEW);
CALLTREE_FUNCTION(wrap__GTK_WINDOW);
CALLTREE_FUNCTION(wrap__g_signal_connect);

CALLTREE_CONSTANT(FALSE);
CALLTREE_CONSTANT(G_APPLICATION_DEFAULT_FLAGS);
CALLTREE_CONSTANT(GTK_ALIGN_START);
CALLTREE_CONSTANT(GTK_ALIGN_CENTER);
CALLTREE_CONSTANT(GTK_ALIGN_END);
CALLTREE_CONSTANT(GTK_ORIENTATION_HORIZONTAL);
CALLTREE_CONSTANT(GTK_ORIENTATION_VERTICAL);
CALLTREE_CONSTANT(GTK_PACK_START);
CALLTREE_CONSTANT(GTK_PACK_END);
CALLTREE_CONSTANT(GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
CALLTREE_CONSTANT(GTK_WRAP_WORD);
CALLTREE_CONSTANT(TRUE);
CALLTREE_CONSTANT(nullptr);

CALLTREE_TYPE(GdkModifierType);
CALLTREE_TYPE(GtkApplication*);
CALLTREE_TYPE(GtkButton*);
CALLTREE_TYPE(GtkEntry*);
CALLTREE_TYPE(GtkEventControllerKey*);
CALLTREE_TYPE(GtkGestureDrag*);
CALLTREE_TYPE(GtkGestureClick*);
CALLTREE_TYPE(GtkNotebook*);
CALLTREE_TYPE(GtkRange*);
CALLTREE_TYPE(GtkSpinButton*);
CALLTREE_TYPE(GtkTextBuffer*);
CALLTREE_TYPE(GtkTextExtendSelection);
CALLTREE_TYPE(const GtkTextIter*);
CALLTREE_TYPE(GtkTextMark*);
CALLTREE_TYPE(GtkTextView*);
CALLTREE_TYPE(GtkWidget*);
CALLTREE_TYPE(gboolean);
CALLTREE_TYPE(gdouble);
CALLTREE_TYPE(gint);
CALLTREE_TYPE(guint);
CALLTREE_TYPE(void);

}

}

namespace gtk_html
{

namespace internal
{

using namespace call_trees;
using namespace html_cpp;

enum class gtk_html_error
{
    NO_ERROR,
    NEITHER_TAG_NOR_TEXT,
    UNKNOWN_TAG,
    ROOT_NOT_SINGLE,
    TITLE_ATTR_NOT_STRING,
    ID_ATTR_NOT_STRING,
    HANDLER_NOT_STRING,
    HANDLER_CONTENT_NOT_STRING,
    HANDLER_TAG_BASE_ATTR_NOT_STRING,
    INPUT_TYPE_NOT_STRING,
    INPUT_VALUE_NOT_STRING,
    INPUT_MIN_NOT_DOUBLE,
    INPUT_MAX_NOT_DOUBLE,
    INPUT_STEP_NOT_DOUBLE,
    INPUT_VALUE_NOT_DOUBLE,
    WIDTH_NOT_INT,
    HEIGHT_NOT_INT,
    X_NOT_INT,
    Y_NOT_INT,
    TD_TAG_ATTR_ROWSPAN_NOT_INT,
    TD_TAG_ATTR_COLSPAN_NOT_INT,
    APPLICATION_TAG_ID_ATTR_MISSING,
    UNKNOWN_INPUT_TYPE,
    IMG_TAG_SRC_ATTR_NOT_STRING,
    TABLE_HAS_TEXT,
    TABLE_HAS_NOT_TR,
    TR_HAS_NOT_TD,
    OVERLAY_EMPTY_BODY,
    STYLE_NOT_STRING,
    ALIGN_NOT_STRING,
    INVALID_ALIGN,
    VALIGN_NOT_STRING,
    INVALID_VALIGN,
    OVERFLOW_NOT_STRING, //XXX: should be css property
    TEXTAREA_CONTENT_NOT_STRING,
};

template<typename Parser>
struct GtkHtmlHandler : Parser
{

template<typename T>
constexpr T* alloc(int n = 1)
{
    return Parser::template alloc<T>(n);
}

constexpr static int strcmp(const char* a, const char* b)
{
    return Parser::strcmp(a, b);
}

constexpr static int strlen(const char* s)
{
    return Parser::strlen(s);
}

constexpr char* strdup(const char* s)
{
    return Parser::strdup(s);
}

constexpr char* str_concat(const char* a, const char* b)
{
    if(!a || !b)
        return nullptr;
    return Parser::str_concat(a, b);
}

constexpr static bool isspace(char c)
{
    return Parser::isspace(c);
}

gtk_html_error gtk_error = gtk_html_error::NO_ERROR;

constexpr void set_error(gtk_html_error err)
{
    if(gtk_error == gtk_html_error::NO_ERROR)
        gtk_error = err;
}

constexpr call_tree* implement_node(html_node* node)
{
    call_tree* ans = nullptr;
    if(node->type == html_node_type::TEXT_NODE)
        ans = implement_text_node(node);
    else if(node->type == html_node_type::START_TAG)
        ans = implement_tag(node);
    else
    {
        set_error(gtk_html_error::NEITHER_TAG_NOR_TEXT);
        return nullptr;
    }
    if(ans)
        ans = set_id(ans, node);
    return ans;
}

constexpr call_tree* parse_and_implement(const char* code)
{
    bool ok = false;
    html_body* ans = Parser::parse(code, ok);
    if(!ok)
        return nullptr;
    if(!ans || ans->next)
    {
        set_error(gtk_html_error::ROOT_NOT_SINGLE);
        return nullptr;
    }
    call_tree* ans1 = implement_node(ans->cur);
    if(ans1 != nullptr)
    {
        int q = ans1 - Parser::allocator_of_call_tree.data;
    }
    return ans1;
}

template<typename... Args>
struct ArgsPack
{
    static constexpr int len = 0;
    static constexpr bool write(call_tree**& p)
    {
        *p++ = nullptr;
        return true;
    }
};

template<typename F, typename... Args>
struct ArgsPack<F, Args...>
{
    static constexpr int len = ArgsPack<Args...>::len + 1;
    static constexpr bool write(call_tree**& p, F q, Args... args)
    {
        if(!q)
            return false;
        *p++ = q;
        return ArgsPack<Args...>::write(p, args...);
    }
};

template<typename... Args>
constexpr call_tree** pack_args(Args... args)
{
    call_tree** ans = alloc<call_tree*>(ArgsPack<Args...>::len+1);
    if(!ans)
        return nullptr;
    call_tree** p = ans;
    if(!ArgsPack<Args...>::write(p, args...))
        return nullptr;
    return ans;
}

constexpr call_tree* make_function_call(const char* fname, call_tree** args)
{
    if(!fname || !args)
        return nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::FUNCTION_CALL;
    ans->fn.name = strdup(fname);
    ans->fn.args = args;
    if(!ans->fn.name)
        return nullptr;
    return ans;
}

constexpr call_tree* make_method_call(const char* fname, call_tree** args, const char* result_id = nullptr)
{
    if(!args)
        return nullptr;
    call_tree* self = args[0];
    args[0] = make_arg(0);
    for(int i = 1; args[i]; i++)
        args[i] = make_shift(args[i]);
    call_tree* inner = make_function_call(fname, args);
    if(result_id)
        inner = make_byid(result_id, inner, 1);
    return make_unshift(self, make_comma(inner, args[0]));
}

//TODO: avoid explicit casts on each method invocation
constexpr call_tree* make_method_cast_call(const char* fname, const char* ftype, call_tree** args)
{
    if(!args)
        return nullptr;
    call_tree* self = args[0];
    call_tree* arg0 = make_arg(0);
    args[0] = make_function_call(ftype, pack_args(arg0));
    for(int i = 1; args[i]; i++)
        args[i] = make_shift(args[i]);
    return make_unshift(self, make_comma(make_function_call(fname, args), arg0));
}

constexpr call_tree* make_g_signal_connect(call_tree* a, call_tree* b, call_tree* c, call_tree* d, const char* put_id)
{
    c = make_function_call("wrap__G_CALLBACK", pack_args(c));
    return make_method_call("wrap__g_signal_connect", pack_args(a, b, c, d), put_id);
}

constexpr call_tree* make_string(const char* s)
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::STR_CONST;
    ans->value_s = strdup(s);
    if(!ans->value_s)
        return nullptr;
    return ans;
}

constexpr call_tree* make_int(int n)
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::INT_CONST;
    ans->value_i = n;
    return ans;
}

constexpr call_tree* make_named(const char* name)
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::NAMED_CONST;
    ans->value_s = strdup(name);
    if(!ans->value_s)
        return nullptr;
    return ans;
}

constexpr call_tree* make_double(double value)
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::DOUBLE_CONST;
    ans->value_d = value;
    return ans;
}

constexpr call_tree* make_arg(int which)
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::ARG_IDX;
    ans->value_i = which;
    return ans;
}

constexpr call_tree* make_shift(call_tree* child)
{
    if(!child)
        return nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::ARG_SHIFT;
    ans->cm.a = child;
    return ans;
}

constexpr call_tree* make_shifts(call_tree* child, int n)
{
    for(int i = 0; i < n; i++)
        child = make_shift(child);
    return child;
}

constexpr call_tree* make_unshift(call_tree* value, call_tree* child)
{
    if(!value || !child)
        return nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::ARG_UNSHIFT;
    ans->cm.a = value;
    ans->cm.b = child;
    return ans;
}

constexpr call_tree* make_comma(call_tree* a, call_tree* b)
{
    if(!a || !b)
        return nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::COMMA;
    ans->cm.a = a;
    ans->cm.b = b;
    return ans;
}

template<typename... A>
constexpr call_tree* make_callback(const char* fn, A... args)
{
    const char* data[] = {args...};
    const char** dyndata = alloc<const char*>(sizeof(data)/sizeof(*data)+1);
    if(!dyndata)
        return nullptr;
    for(int i = 0; i < sizeof(data)/sizeof(*data); i++)
    {
        dyndata[i] = strdup(data[i]);
        if(!dyndata[i])
            return nullptr;
    }
    dyndata[sizeof(data)/sizeof(*data)] = nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::CALLBACK;
    ans->cb.name = strdup(fn);
    if(!ans->cb.name)
        return nullptr;
    ans->cb.args = dyndata;
    return ans;
}

constexpr call_tree* make_callback_uap()
{
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::CALLBACK_UAP;
    return ans;
}

constexpr call_tree* make_byid(const char* id, call_tree* self, int pops = 0)
{
    if(!self)
        return nullptr;
    call_tree* ans = alloc<call_tree>();
    if(!ans)
        return nullptr;
    ans->type = call_tree_type::PUT_ID;
    ans->byid.id = strdup(id);
    if(!ans->byid.id)
        return nullptr;
    ans->byid.value = self;
    ans->byid.nshift = pops;
    return ans;
}

constexpr call_tree* implement_text_node(html_node* node)
{
    return make_function_call("gtk_label_new", pack_args(make_string(node->text_node)));
}

constexpr bool check_attr(html_node* node, const char* name)
{
    for(html_attribute* attr = node->tag.attrs; attr; attr = attr->next)
        if(!strcmp(attr->name, name))
            return true;
    return false;
}

constexpr html_node* get_attr_raw(html_node* node, const char* name)
{
    for(html_attribute* attr = node->tag.attrs; attr; attr = attr->next)
        if(!strcmp(attr->name, name))
            return attr->value;
    return nullptr;
}

constexpr const char* get_attr(html_node* node, const char* name, gtk_html_error err)
{
    for(html_attribute* attr = node->tag.attrs; attr; attr = attr->next)
        if(!strcmp(attr->name, name))
        {
            if(attr->value->type != html_node_type::TEXT_NODE)
            {
                set_error(err);
                return nullptr;
            }
            return attr->value->text_node;
        }
    return nullptr;
}

constexpr call_tree* implement_tag(html_node* node)
{
    if(!strcmp(node->tag.name, "div") || !strcmp(node->tag.name, "span"))
        return implement_tag_div_span(node);
    if(!strcmp(node->tag.name, "window"))
        return implement_tag_window(node);
    if(!strcmp(node->tag.name, "application"))
        return implement_tag_application(node);
    if(!strcmp(node->tag.name, "button"))
        return implement_tag_button(node);
    if(!strcmp(node->tag.name, "input"))
        return implement_tag_input(node);
    if(!strcmp(node->tag.name, "textarea"))
        return implement_tag_textarea(node);
    if(!strcmp(node->tag.name, "popover"))
        return implement_tag_simple(node, "popover");
    if(!strcmp(node->tag.name, "fixed"))
        return implement_tag_fixed(node);
    if(!strcmp(node->tag.name, "img"))
        return implement_tag_img(node);
    if(!strcmp(node->tag.name, "widget"))
        return implement_tag_widget(node);
    if(!strcmp(node->tag.name, "table"))
        return implement_tag_table(node);
    if(!strcmp(node->tag.name, "overlay"))
        return implement_tag_overlay(node);
    if(!strcmp(node->tag.name, "notebook"))
        return implement_tag_notebook(node);
    if(!strcmp(node->tag.name, "stack"))
        return implement_tag_stack(node);
    set_error(gtk_html_error::UNKNOWN_TAG);
    return nullptr;
}

constexpr call_tree* bind_mouse_events(call_tree* widget, html_node* node, int pop=0)
{
    if(get_attr_raw(node, "onmousedown") || get_attr_raw(node, "onmousemove") || get_attr_raw(node, "mouseup"))
    {
        call_tree* drag = make_function_call("gtk_gesture_drag_new", pack_args());
        drag = maybe_callback_pop(drag, node, "onmousedown", "drag-begin", pop, "void", "GtkGestureDrag*", "gdouble", "gdouble");
        drag = maybe_callback_pop(drag, node, "onmousemove", "drag-update", pop, "void", "GtkGestureDrag*", "gdouble", "gdouble");
        drag = maybe_callback_pop(drag, node, "onmouseup", "drag-end", pop, "void", "GtkGestureDrag*", "gdouble", "gdouble");
        widget = make_method_call("gtk_widget_add_controller", pack_args(widget, make_function_call("wrap__GTK_EVENT_CONTROLLER", pack_args(drag))));
    }
    if(get_attr_raw(node, "onkeydown") || get_attr_raw(node, "onkeyup"))
    {
        call_tree* eck = make_function_call("gtk_event_controller_key_new", pack_args());
        eck = maybe_callback_pop(eck, node, "onkeydown", "key-pressed", pop, "void", "GtkEventControllerKey*", "guint", "guint", "GdkModifierType");
        eck = maybe_callback_pop(eck, node, "onkeyup", "key-released", pop, "void", "GtkEventControllerKey*", "guint", "guint", "GdkModifierType");
        widget = make_method_call("gtk_widget_add_controller", pack_args(widget, make_function_call("wrap__GTK_EVENT_CONTROLLER", pack_args(eck))));
    }
    if(get_attr_raw(node, "onclick") && !has_native_onclick(node))
    {
        call_tree* click = make_function_call("gtk_gesture_click_new", pack_args());
        click = maybe_callback_pop(click, node, "onclick", "pressed", pop, "void", "GtkGestureClick*", "gint", "gdouble", "gdouble");
        widget = make_method_call("gtk_widget_add_controller", pack_args(widget, make_function_call("wrap__GTK_EVENT_CONTROLLER", pack_args(click))));
    }
    if(get_attr_raw(node, "oncontextmenu"))
    {
        call_tree* click = make_function_call("gtk_gesture_click_new", pack_args());
        click = make_method_cast_call("gtk_gesture_single_set_button", "wrap__GTK_GESTURE_SINGLE", pack_args(click, make_int(3)));
        click = maybe_callback_pop(click, node, "oncontextmenu", "pressed", pop, "void", "GtkGestureClick*", "gint", "gdouble", "gdouble");
        widget = make_method_call("gtk_widget_add_controller", pack_args(widget, make_function_call("wrap__GTK_EVENT_CONTROLLER", pack_args(click))));
    }
    return widget;
}

constexpr call_tree* set_align(call_tree* widget, html_node* node)
{
    const char* align = get_attr(node, "align", gtk_html_error::ALIGN_NOT_STRING);
    if(!align){}
    else if(!strcmp(align, "left"))
        widget = make_method_call("gtk_widget_set_halign", pack_args(widget, make_named("GTK_ALIGN_START")));
    else if(!strcmp(align, "center"))
        widget = make_method_call("gtk_widget_set_halign", pack_args(widget, make_named("GTK_ALIGN_CENTER")));
    else if(!strcmp(align, "right"))
        widget = make_method_call("gtk_widget_set_halign", pack_args(widget, make_named("GTK_ALIGN_END")));
    else
    {
        set_error(gtk_html_error::INVALID_ALIGN);
        return nullptr;
    }
    const char* valign = get_attr(node, "valign", gtk_html_error::VALIGN_NOT_STRING);
    if(!valign){}
    else if(!strcmp(valign, "top"))
        widget = make_method_call("gtk_widget_set_valign", pack_args(widget, make_named("GTK_ALIGN_START")));
    else if(!strcmp(valign, "middle"))
        widget = make_method_call("gtk_widget_set_valign", pack_args(widget, make_named("GTK_ALIGN_CENTER")));
    else if(!strcmp(valign, "bottom"))
        widget = make_method_call("gtk_widget_set_valign", pack_args(widget, make_named("GTK_ALIGN_END")));
    else
    {
        set_error(gtk_html_error::INVALID_VALIGN);
        return nullptr;
    }
    return widget;
}

constexpr call_tree* set_expands(call_tree* widget, html_node* node)
{
    if(get_attr_raw(node, "vexpand"))
        widget = make_method_call("gtk_widget_set_vexpand", pack_args(widget, make_named("TRUE")));
    if(get_attr_raw(node, "hexpand"))
        widget = make_method_call("gtk_widget_set_hexpand", pack_args(widget, make_named("TRUE")));
    widget = set_align(widget, node);
    bool have1 = false, have2 = false;
    int width = get_attr_int(node, "width", gtk_html_error::WIDTH_NOT_INT, 0, have1);
    int height = get_attr_int(node, "height", gtk_html_error::HEIGHT_NOT_INT, 0, have2);
    if(have1 || have2)
        widget = make_method_call("gtk_widget_set_size_request", pack_args(
            widget,
            make_int(width),
            make_int(height)
        ));
    const char* style = get_attr(node, "style", gtk_html_error::STYLE_NOT_STRING);
    if(style)
    {
        style = strdup(style);
        if(!style)
            return nullptr;
        style = Parser::trim(strdup(style));
        if(!strlen(style))
            style = nullptr;
    }
    if(style)
    {
        if(style[strlen(style)-1] != ';')
            style = str_concat(style, ";");
        style = str_concat("* { ", str_concat(style, " }"));
        if(!style)
            return nullptr;
        call_tree* provider = make_function_call("gtk_css_provider_new", pack_args());
        provider = make_method_call("gtk_css_provider_load_from_data", pack_args(
            provider,
            make_string(style),
            make_int(strlen(style))
        ));
        provider = make_function_call("wrap__GTK_STYLE_PROVIDER", pack_args(provider));
        call_tree* ctx = make_function_call("gtk_widget_get_style_context", pack_args(make_arg(0)));
        ctx = make_function_call("gtk_style_context_add_provider", pack_args(
            ctx,
            provider,
            make_named("GTK_STYLE_PROVIDER_PRIORITY_APPLICATION")
        ));
        widget = make_unshift(widget, make_comma(ctx, make_arg(0)));
    }
    return widget;
}

constexpr call_tree* implement_tag_window(html_node* node)
{
    call_tree* win = make_function_call("gtk_application_window_new", pack_args(make_arg(0)));
    const char* title = get_attr(node, "title", gtk_html_error::TITLE_ATTR_NOT_STRING);
    if(title)
        win = make_method_cast_call("gtk_window_set_title", "wrap__GTK_WINDOW", pack_args(win, make_string(title)));
    if(check_attr(node, "modal"))
        win = make_method_cast_call("gtk_window_set_modal", "wrap__GTK_WINDOW", pack_args(win, make_int(1)));
    if(check_attr(node, "noresize"))
        win = make_method_cast_call("gtk_window_set_resizable", "wrap__GTK_WINDOW", pack_args(win, make_int(0)));
    if(check_attr(node, "fullscreen"))
        win = make_method_cast_call("gtk_window_fullscreen", "wrap__GTK_WINDOW", pack_args(win));
    if(check_attr(node, "show"))
        win = make_method_call("gtk_widget_show", pack_args(win));
    win = add_boxed_children("gtk_window_set_child", "wrap__GTK_WINDOW", node, win, 1);
    return set_id(set_expands(bind_mouse_events(win, node, 1), node), node, 1);
}

constexpr call_tree* implement_tag_application(html_node* node)
{
    const char* id = get_attr(node, "id", gtk_html_error::ID_ATTR_NOT_STRING);
    if(!id)
    {
        set_error(gtk_html_error::APPLICATION_TAG_ID_ATTR_MISSING);
        return nullptr;
    }
    call_tree* app = make_function_call("gtk_application_new", pack_args(make_string(id), make_named("G_APPLICATION_DEFAULT_FLAGS")));
    app = maybe_callback(app, node, "onactivate", "activate", "void", "GtkApplication*");
    if(check_attr(node, "run"))
        app = make_method_cast_call("g_application_run", "wrap__G_APPLICATION", pack_args(app, make_int(0), make_named("nullptr")));
    return app;
}

constexpr void box_append(call_tree*& q, bool& o, call_tree* r, const char* orientation)
{
    if(q == nullptr)
        q = r;
    else
    {
        if(o)
        {
            o = false;
            call_tree* b = make_function_call("gtk_box_new", pack_args(make_named(orientation), make_int(0)));
            q = make_method_cast_call("gtk_box_append", "wrap__GTK_BOX", pack_args(b, q));
        }
        q = make_method_cast_call("gtk_box_append", "wrap__GTK_BOX", pack_args(q, r));
    }
}

constexpr call_tree* box_if_null(call_tree* q, const char* orientation)
{
    if(!q)
        return make_function_call("gtk_box_new", pack_args(make_named(orientation), make_int(0)));
    return q;
}

constexpr call_tree* implement_tag_div_span(html_node* node)
{
    call_tree* box = nullptr;
    bool box_only = true;
    for(html_body* q = node->tag.body; q; q = q->next)
    {
        call_tree* box_pre = nullptr;
        bool box_pre_only = true;
        while(q && (q->cur->type != html_node_type::START_TAG || strcmp(q->cur->tag.name, "div")))
        {
            box_append(box_pre, box_pre_only, implement_node(q->cur), "GTK_ORIENTATION_HORIZONTAL");
            q = q->next;
        }
        if(box_pre)
            box_append(box, box_only, box_pre, "GTK_ORIENTATION_VERTICAL");
        if(!q)
            break;
        box_append(box, box_only, implement_node(q->cur), "GTK_ORIENTATION_VERTICAL");
    }
    const char* ovf = get_attr(node, "overflow", gtk_html_error::OVERFLOW_NOT_STRING);
    box = box_if_null(box, "GTK_ORIENTATION_VERTICAL");
    if(ovf && !strcmp(ovf, "scroll"))
    {
        call_tree* win = make_function_call("gtk_scrolled_window_new", pack_args());
        box = make_method_cast_call("gtk_scrolled_window_set_child", "wrap__GTK_SCROLLED_WINDOW", pack_args(win, box));
    }
    return set_align(bind_mouse_events(box, node), node);
}

constexpr call_tree* add_boxed_children(const char* method, const char* cast, html_node* node, call_tree* obj, int pop_args = 0)
{
    if(node->tag.body)
    {
        call_tree* box = make_shifts(implement_tag_div_span(node), pop_args);
        if(cast)
            obj = make_method_cast_call(method, cast, pack_args(obj, box));
        else
            obj = make_method_call(method, pack_args(obj, box));
    }
    return obj;
}

constexpr bool has_native_onclick(html_node* node)
{
    return node->type == html_node_type::START_TAG && !strcmp(node->tag.name, "button");
}

constexpr call_tree* implement_tag_button(html_node* node)
{
    call_tree* btn = make_function_call("gtk_button_new", pack_args());
    btn = add_boxed_children("gtk_button_set_child", "wrap__GTK_BUTTON", node, btn);
    btn = maybe_callback(btn, node, "onclick", "clicked", "void", "GtkButton*");
    btn = maybe_callback(btn, node, "onactivate", "activate", "void", "GtkButton*");
    return set_expands(bind_mouse_events(btn, node), node);
}

constexpr call_tree* implement_input_text(html_node* node, bool is_password)
{
    const char* value = get_attr(node, "value", gtk_html_error::INPUT_VALUE_NOT_STRING);
    call_tree* ans = make_function_call("gtk_entry_new", pack_args());
    if(value)
    {
        call_tree* arg0 = make_arg(0);
        call_tree* set_text = make_function_call("gtk_entry_buffer_set_text", pack_args(
            make_function_call("gtk_entry_get_buffer", pack_args(
                make_function_call("wrap__GTK_ENTRY", pack_args(arg0))
            )),
            make_string(value),
            make_int(strlen(value))
        ));
        call_tree* comma = make_comma(set_text, arg0);
        ans = make_unshift(ans, comma);
    }
    ans = maybe_callback(ans, node, "onactivate", "activate", "void", "GtkEntry*");
    if(is_password)
        ans = make_method_cast_call("gtk_entry_set_visibility", "wrap__GTK_ENTRY", pack_args(ans, make_named("FALSE")));
    return ans;
}

static constexpr int parse_int(const char* s, bool& ok)
{
    ok = true;
    while(isspace(*s))
        s++;
    bool neg = false;
    int ans = 0;
    if(*s == '+')
        s++;
    else if(*s == '-')
    {
        s++;
        neg = true;
    }
    const char* s0 = s;
    while(*s >= '0' && *s <= '9')
    {
        ans = 10 * ans + (*s - '0');
        s++;
    }
    if(s == s0)
    {
        ok = false;
        return 0;
    }
    while(isspace(*s))
        s++;
    if(*s)
    {
        ok = false;
        return 0;
    }
    if(neg)
        ans = -ans;
    return ans;
}

static constexpr double parse_double(const char* s, bool& ok)
{
    ok = true;
    while(isspace(*s))
        s++;
    bool neg = false;
    if(*s == '-')
    {
        s++;
        neg = true;
    }
    else if(*s == '+')
        s++;
    double ans = 0;
    bool digits = false;
    while(*s >= '0' && *s <= '9')
    {
        ans = 10 * ans + (*s - '0');
        s++;
        digits = true;
    }
    if(*s == '.')
    {
        double div = 1;
        s++;
        while(*s >= '0' && *s <= '9')
        {
            div *= 10;
            ans += (*s - '0') / div;
            s++;
            digits = true;
        }
    }
    if(!digits)
    {
        ok = false;
        return 0;
    }
    if(*s == 'e')
    {
        s++;
        bool neg2 = false;
        if(*s == '-')
        {
            neg2 = true;
            s++;
        }
        else if(*s == '+')
            s++;
        int q = 0;
        if(!(*s >= '0' && *s <= '9'))
        {
            ok = false;
            return 0;
        }
        while(*s >= '0' && *s <= '9')
        {
            q = 10 * q + (*s - '0');
            s++;
        }
        if(neg2)
        {
            for(int i = 0; i < q; i++)
                ans /= 10;
        }
        else
        {
            for(int i = 0; i < q; i++)
                ans *= 10;
        }
    }
    while(isspace(*s))
        s++;
    if(*s)
    {
        ok = false;
        return 0;
    }
    if(neg)
        ans = -ans;
    return ans;
}

constexpr int get_attr_int(html_node* node, const char* attr, gtk_html_error err, int def, bool& have)
{
    const char* s = get_attr(node, attr, err);
    if(!s)
    {
        have = false;
        return def;
    }
    have = true;
    bool ok = true;
    int ans = parse_int(s, ok);
    if(!ok)
        set_error(err);
    return ans;
}

constexpr double get_attr_double(html_node* node, const char* attr, gtk_html_error err, double def, bool& have)
{
    const char* s = get_attr(node, attr, err);
    if(!s)
    {
        have = false;
        return def;
    }
    have = true;
    bool ok = true;
    double ans = parse_double(s, ok);
    if(!ok)
        set_error(err);
    return ans;
}

constexpr call_tree* implement_input_number(html_node* node)
{
    bool have = false;
    call_tree* ans = make_function_call("gtk_spin_button_new_with_range", pack_args(
        make_double(get_attr_double(node, "min", gtk_html_error::INPUT_MIN_NOT_DOUBLE, -1e308, have)),
        make_double(get_attr_double(node, "max", gtk_html_error::INPUT_MAX_NOT_DOUBLE, 1e308, have)),
        make_double(get_attr_double(node, "step", gtk_html_error::INPUT_STEP_NOT_DOUBLE, 1, have))
    ));
    double value = get_attr_double(node, "value", gtk_html_error::INPUT_VALUE_NOT_DOUBLE, 0, have);
    if(have)
        ans = make_method_cast_call("gtk_spin_button_set_value", "wrap__GTK_SPIN_BUTTON", pack_args(
            ans,
            make_double(value)
        ));
    ans = maybe_callback(ans, node, "onchange", "value_changed", "void", "GtkSpinButton*");
    return ans;
}

constexpr call_tree* implement_input_range(html_node* node)
{
    bool have = false;
    call_tree* ans = make_function_call("gtk_scale_new_with_range", pack_args(
        make_named("GTK_ORIENTATION_HORIZONTAL"),
        make_double(get_attr_double(node, "min", gtk_html_error::INPUT_MIN_NOT_DOUBLE, 0, have)),
        make_double(get_attr_double(node, "max", gtk_html_error::INPUT_MAX_NOT_DOUBLE, 100, have)),
        make_double(get_attr_double(node, "step", gtk_html_error::INPUT_STEP_NOT_DOUBLE, 1, have))
    ));
    double value = get_attr_double(node, "value", gtk_html_error::INPUT_VALUE_NOT_DOUBLE, 0, have);
    if(have)
        ans = make_method_cast_call("gtk_range_set_value", "wrap__GTK_RANGE", pack_args(
            ans,
            make_double(value)
        ));
    ans = maybe_callback(ans, node, "onchange", "value_changed", "void", "GtkRange*");
    return ans;
}

constexpr call_tree* implement_tag_input(html_node* node)
{
    call_tree* ans = nullptr;
    const char* type = get_attr(node, "type", gtk_html_error::INPUT_TYPE_NOT_STRING);
    if(!type || !strcmp(type, "text") || !strcmp(type, "password"))
        ans = implement_input_text(node, type && !strcmp(type, "password"));
    else if(!strcmp(type, "number"))
        ans = implement_input_number(node);
    else if(!strcmp(type, "range"))
        ans = implement_input_range(node);
    else
        set_error(gtk_html_error::UNKNOWN_INPUT_TYPE);
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* implement_tag_textarea(html_node* node)
{
    const char* content = nullptr;
    if(node->tag.body)
    {
        if(node->tag.body->next || node->tag.body->cur->type != html_node_type::TEXT_NODE)
        {
            set_error(gtk_html_error::TEXTAREA_CONTENT_NOT_STRING);
            return nullptr;
        }
        content = node->tag.body->cur->text_node;
    }
    call_tree* ans = make_function_call("gtk_text_view_new", pack_args());
    if(get_attr_raw(node, "readonly"))
    {
        ans = make_method_cast_call("gtk_text_view_set_editable", "wrap__GTK_TEXT_VIEW", pack_args(ans, make_named("FALSE")));
        ans = make_method_cast_call("gtk_text_view_set_cursor_visible", "wrap__GTK_TEXT_VIEW", pack_args(ans, make_named("FALSE")));
    }
    if(get_attr_raw(node, "wrap"))
        ans = make_method_cast_call("gtk_text_view_set_wrap_mode", "wrap__GTK_TEXT_VIEW", pack_args(ans, make_named("GTK_WRAP_WORD")));
    if(content || get_attr_raw(node, "onbeginuseraction") || get_attr_raw(node, "onenduseraction") || get_attr_raw(node, "onmarkset"))
    {
        call_tree* arg0 = make_arg(0);
        call_tree* buf = make_function_call("gtk_text_view_get_buffer", pack_args(make_function_call("wrap__GTK_TEXT_VIEW", pack_args(arg0))));
        if(content)
            buf = make_method_call("gtk_text_buffer_set_text", pack_args(buf, make_string(content), make_int(strlen(content))));
        buf = maybe_callback_pop(buf, node, "onbeginuseraction", "begin-user-action", 1, "void", "GtkTextBuffer*");
        buf = maybe_callback_pop(buf, node, "onenduseraction", "end-user-action", 1, "void", "GtkTextBuffer*");
        buf = maybe_callback_pop(buf, node, "onmarkset", "end-user-action", 1, "void", "GtkTextBuffer*", "const GtkTextIter*", "GtkTextMark*");
        ans = make_unshift(ans, make_comma(buf, arg0));
    }
    return ans;
}

constexpr char* capitalize(const char* s)
{
    int l = strlen(s);
    char* ans = alloc<char>(l+1);
    for(int i = 0; i <= l; i++)
        if(s[i] >= 'a' && s[i] <= 'z')
            ans[i] = s[i] - 'a' + 'A';
        else
            ans[i] = s[i];
    return ans;
}

constexpr call_tree* implement_tag_simple(html_node* node, const char* name)
{
    call_tree* ans = make_function_call(str_concat("gtk_", str_concat(name, "_new")), pack_args());
    ans = add_boxed_children(
        str_concat("gtk_", str_concat(name, "_set_child")),
        str_concat("wrap__GTK_", capitalize(name)),
        node,
        ans
    );
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* implement_tag_fixed(html_node* node)
{
    call_tree* ans = make_function_call("gtk_fixed_new", pack_args());
    for(html_body* cur = node->tag.body; cur; cur = cur->next)
    {
        int x = 0;
        int y = 0;
        if(cur->cur->type == html_node_type::START_TAG)
        {
            bool have = false;
            x = get_attr_int(cur->cur, "x", gtk_html_error::X_NOT_INT, 0, have);
            y = get_attr_int(cur->cur, "y", gtk_html_error::Y_NOT_INT, 0, have);
        }
        ans = make_method_cast_call("gtk_fixed_put", "wrap__GTK_FIXED", pack_args(
            ans,
            implement_node(cur->cur),
            make_int(x),
            make_int(y)
        ));
    }
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* implement_tag_img(html_node* node)
{
    //TODO: tag as src
    const char* src = get_attr(node, "src", gtk_html_error::IMG_TAG_SRC_ATTR_NOT_STRING);
    call_tree* ans = nullptr;
    if(src)
        ans = make_function_call("gtk_picture_new_for_filename", pack_args(make_string(src)));
    else
        ans = make_function_call("gtk_picture_new", pack_args(make_string(src)));
    ans = make_method_cast_call("gtk_picture_set_can_shrink", "wrap__GTK_PICTURE", pack_args(ans, make_named("FALSE")));
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* implement_tag_widget(html_node* node)
{
    return set_id(set_expands(bind_mouse_events(make_arg(0), node, 1), node), node, 1);
}

constexpr call_tree* implement_tag_table(html_node* node)
{
    int n = 0;
    for(html_body* i = node->tag.body; i; i = i->next)
    {
        n++;
        html_node* tag = i->cur;
        if(tag->type != html_node_type::START_TAG)
        {
            set_error(gtk_html_error::TABLE_HAS_TEXT);
            return nullptr;
        }
        if(strcmp(tag->tag.name, "tr"))
        {
            set_error(gtk_html_error::TABLE_HAS_NOT_TR);
            return nullptr;
        }
    }
    int* arr = alloc<int>(n);
    if(!arr)
        return nullptr;
    html_body** arr2 = alloc<html_body*>(n);
    if(!arr2)
        return nullptr;
    int j = 0;
    for(html_body* i = node->tag.body; i; i = i->next)
    {
        arr[j] = 0;
        arr2[j++] = i->cur->tag.body;
    }
    int x = 0;
    int m = n;
    call_tree* grid = make_function_call("gtk_grid_new", pack_args());
    while(m)
    {
        int lim = 0;
        int x1 = x;
        for(int i = 0; i < n; i++)
        {
            if(i < lim)
            {
                if(arr[i] > x1)
                    x1 = arr[i];
                continue;
            }
            if(arr[i] > x)
                continue;
            if(!arr2[i])
                continue;
            bool have = false;
            if(arr2[i]->cur->type != html_node_type::START_TAG || strcmp(arr2[i]->cur->tag.name, "td"))
            {
                set_error(gtk_html_error::TR_HAS_NOT_TD);
                return nullptr;
            }
            int rs = get_attr_int(arr2[i]->cur, "rowspan", gtk_html_error::TD_TAG_ATTR_ROWSPAN_NOT_INT, 1, have);
            lim = i + rs;
        }
        if(x1 > x)
        {
            x = x1;
            continue;
        }
        lim = 0;
        int limx = 0;
        m = 0;
        for(int i = 0; i < n; i++)
        {
            if(arr2[i])
                m++;
            if(i < lim)
            {
                arr[i] = limx;
                continue;
            }
            if(arr[i] > x || !arr2[i])
                continue;
            html_body* p_tag = arr2[i];
            arr2[i] = p_tag->next;
            html_node* tag = p_tag->cur;
            bool have = false;
            int rs = get_attr_int(tag, "rowspan", gtk_html_error::TD_TAG_ATTR_ROWSPAN_NOT_INT, 1, have);
            int cs = get_attr_int(tag, "colspan", gtk_html_error::TD_TAG_ATTR_COLSPAN_NOT_INT, 1, have);
            limx = x + cs;
            lim = i + rs;
            arr[i] = limx;
            int y = i;
            grid = make_method_cast_call("gtk_grid_attach", "wrap__GTK_GRID", pack_args(
                grid,
                set_id(implement_tag_div_span(tag), tag),
                make_int(x),
                make_int(y),
                make_int(cs),
                make_int(rs)
            ));
        }
        x++;
    }
    return set_expands(bind_mouse_events(grid, node), node);
}

constexpr call_tree* implement_tag_overlay(html_node* node)
{
    if(!node->tag.body)
    {
        set_error(gtk_html_error::OVERLAY_EMPTY_BODY);
        return nullptr;
    }
    call_tree* overlay = make_function_call("gtk_overlay_new", pack_args());
    overlay = make_method_cast_call("gtk_overlay_set_child", "wrap__GTK_OVERLAY", pack_args(overlay, implement_node(node->tag.body->cur)));
    for(html_body* i = node->tag.body->next; i; i = i->next)
    {
        call_tree* elem = implement_node(i->cur);
        overlay = make_method_cast_call("gtk_overlay_add_overlay", "wrap__GTK_OVERLAY", pack_args(overlay, elem));
    }
    return set_expands(bind_mouse_events(overlay, node), node);
}

constexpr call_tree* implement_tag_notebook(html_node* node)
{
    call_tree* ans = make_function_call("gtk_notebook_new", pack_args());
    for(html_body* i = node->tag.body; i; i = i->next)
    {
        call_tree* elem = nullptr;
        call_tree* label = nullptr;
        if(i->cur->type == html_node_type::START_TAG && !strcmp(i->cur->tag.name, "notebook-tab"))
        {
            elem = implement_tag_div_span(i->cur);
            html_node* label_elem = get_attr_raw(i->cur, "label");
            if(label_elem)
                label = implement_node(label_elem);
        }
        else
            elem = implement_node(i->cur);
        if(!label)
            label = make_named("nullptr");
        call_tree* arg0 = make_arg(0);
        call_tree* append_value = make_function_call("gtk_notebook_append_page", pack_args(
            make_function_call("wrap__GTK_NOTEBOOK", pack_args(arg0)),
            make_shift(elem),
            make_shift(label)
        ));
        if(i->cur->type == html_node_type::START_TAG && !strcmp(i->cur->tag.name, "notebook-tab"))
            append_value = set_id(append_value, i->cur, 1);
        ans = make_unshift(ans, make_comma(append_value, arg0));
    }
    html_node* action_start = get_attr_raw(node, "action-start");
    if(action_start)
        ans = make_method_cast_call("gtk_notebook_set_action_widget", "wrap__GTK_NOTEBOOK", pack_args(
            ans,
            implement_node(action_start),
            make_named("GTK_PACK_START")
        ));
    html_node* action_end = get_attr_raw(node, "action-end");
    if(action_end)
        ans = make_method_cast_call("gtk_notebook_set_action_widget", "wrap__GTK_NOTEBOOK", pack_args(
            ans,
            implement_node(action_end),
            make_named("GTK_PACK_END")
        ));
    ans = maybe_callback(ans, node, "onchange", "change-current-page", "gboolean", "GtkNotebook*", "gint");
    ans = maybe_callback(ans, node, "onswitchpage", "switch-page", "void", "GtkNotebook*", "GtkWidget*", "guint");
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* implement_tag_stack(html_node* node)
{
    call_tree* ans = make_function_call("gtk_stack_new", pack_args());
    for(html_body* i = node->tag.body; i; i = i->next)
        ans = make_method_cast_call("gtk_stack_add_child", "wrap__GTK_STACK", pack_args(ans, implement_node(i->cur)));
    return set_expands(bind_mouse_events(ans, node), node);
}

constexpr call_tree* set_id(call_tree* self, html_node* node, int pops = 0)
{
    if(node->type != html_node_type::START_TAG)
        return self;
    const char* id = get_attr(node, "id", gtk_html_error::ID_ATTR_NOT_STRING);
    if(id)
    {
        if(self && self->type == call_tree_type::PUT_ID && !strcmp(self->byid.id, id))
            return self;
        self = make_byid(id, self, pops);
    }
    return self;
}

template<typename... Args>
constexpr call_tree* maybe_callback_pop(call_tree* self, html_node* node, const char* attr, const char* signal, int pops, Args... signature)
{
    const char* handler_name = nullptr;
    const char* id_handler = nullptr;
    const char* id_base = nullptr;
    html_node* handler = get_attr_raw(node, attr);
    if(!handler)
        return self;
    if(handler->type == html_node_type::TEXT_NODE)
        handler_name = handler->text_node;
    else if(handler->type == html_node_type::START_TAG)
    {
        if(strcmp(handler->tag.name, "handler"))
        {
            set_error(gtk_html_error::HANDLER_NOT_STRING);
            return nullptr;
        }
        if(!handler->tag.body || handler->tag.body->next || handler->tag.body->cur->type != html_node_type::TEXT_NODE)
        {
            set_error(gtk_html_error::HANDLER_CONTENT_NOT_STRING);
            return nullptr;
        }
        id_handler = get_attr(handler, "id", gtk_html_error::ID_ATTR_NOT_STRING);
        id_base = get_attr(handler, "base", gtk_html_error::HANDLER_TAG_BASE_ATTR_NOT_STRING);
        handler_name = handler->tag.body->cur->text_node;
    }
    if(id_base)
        self = make_byid(id_base, self, pops);
    if(handler_name)
        self = make_g_signal_connect(self, make_string(signal), make_shifts(make_callback(handler_name, signature...), pops), make_shifts(make_callback_uap(), pops), id_handler);
    return self;
}

template<typename... Args>
constexpr call_tree* maybe_callback(call_tree* self, html_node* node, const char* attr, const char* signal, Args... signature)
{
    return maybe_callback_pop(self, node, attr, signal, 0, signature...);
}

};

struct GtkHtmlFactory
{
    template<int memsz>
    struct Impl
        : DoCallTree<GtkHtmlHandler<HTMLParser<CompileTimeAllocator<memsz>>>>
    {
        const Impl* self = this;
        constexpr Impl(const char* s) : DoCallTree<GtkHtmlHandler<HTMLParser<CompileTimeAllocator<memsz>>>>(s){}
    };
};

template<typename S>
struct GtkHtmlImplementation
{
    using tree = Reallocator<GtkHtmlFactory, S>;
    static_assert(!tree::data.self->memory_error, "unhandled OOM during parsing");
    static constexpr parser_errors error = tree::data.self->error_code;
    static_assert(error == parser_errors::NO_ERROR, "syntax error");
    static_assert(error != parser_errors::MEMORY_ERROR, "memory error");
    static_assert(error != parser_errors::TAG_NO_GT, "a tag is missing a closing >");
    static_assert(error != parser_errors::MISMATCHED_END_TAG, "opening and closing tags do not match");
    static_assert(error != parser_errors::TAG_NEVER_CLOSED, "tag never closed");
    static_assert(error != parser_errors::CLOSING_ATTRS, "closing tag cannot have attributes");
    static_assert(error != parser_errors::TAG_NO_QUOT, "missing ending quote in attribute value");
    static_assert(error != parser_errors::INVALID_SELFCLOSE, "malformed self-closing tag");
    static_assert(error != parser_errors::MISSING_ATTR_TAG, "tag-backed attribute has no tag");
    static_assert(error != parser_errors::CLOSING_SELFCLOSE, "closing tag cannot be self-closing");
    static_assert(error != parser_errors::UNTERMINATED_COMMENT, "unterminated comment");
    static constexpr gtk_html_error error2 = tree::data.self->gtk_error;
    static_assert(error2 == gtk_html_error::NO_ERROR, "semantics error");
    static_assert(error2 != gtk_html_error::NEITHER_TAG_NOR_TEXT, "cannot implement a node that is neither a tag nor a text node");
    static_assert(error2 != gtk_html_error::UNKNOWN_TAG, "unknown or out of context tag encountered");
    static_assert(error2 != gtk_html_error::ROOT_NOT_SINGLE, "multiple root tags are not allowed");
    static_assert(error2 != gtk_html_error::TITLE_ATTR_NOT_STRING, "\"title\" attribute must be a string");
    static_assert(error2 != gtk_html_error::ID_ATTR_NOT_STRING, "\"id\" attribute must be a string");
    static_assert(error2 != gtk_html_error::HANDLER_NOT_STRING, "\"on*\" attribute must be a string or a <handler>");
    static_assert(error2 != gtk_html_error::HANDLER_CONTENT_NOT_STRING, "<handler> tag must contain a single non-tag node");
    static_assert(error2 != gtk_html_error::HANDLER_TAG_BASE_ATTR_NOT_STRING, "<handler> \"base\" attribute must be a string");
    static_assert(error2 != gtk_html_error::INPUT_TYPE_NOT_STRING, "<input> \"type\" attribute must be a string");
    static_assert(error2 != gtk_html_error::INPUT_VALUE_NOT_STRING, "<input> \"value\" attribute must be a string");
    static_assert(error2 != gtk_html_error::INPUT_MIN_NOT_DOUBLE, "<input> \"min\" attribute must be a number");
    static_assert(error2 != gtk_html_error::INPUT_MAX_NOT_DOUBLE, "<input> \"max\" attribute must be a number");
    static_assert(error2 != gtk_html_error::INPUT_STEP_NOT_DOUBLE, "<input> \"step\" attribute must be a number");
    static_assert(error2 != gtk_html_error::INPUT_VALUE_NOT_DOUBLE, "<input> \"value\" attribute must be a number");
    static_assert(error2 != gtk_html_error::WIDTH_NOT_INT, "\"width\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::HEIGHT_NOT_INT, "\"height\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::X_NOT_INT, "\"x\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::Y_NOT_INT, "\"y\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::TD_TAG_ATTR_ROWSPAN_NOT_INT, "<td> \"rowspan\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::TD_TAG_ATTR_COLSPAN_NOT_INT, "<td> \"colspan\" attribute must be an integer");
    static_assert(error2 != gtk_html_error::APPLICATION_TAG_ID_ATTR_MISSING, "<application> tag must have \"id\" attribute");
    static_assert(error2 != gtk_html_error::UNKNOWN_INPUT_TYPE, "unknown input type");
    static_assert(error2 != gtk_html_error::IMG_TAG_SRC_ATTR_NOT_STRING, "<img> \"src\" attribute must be a string");
    static_assert(error2 != gtk_html_error::TABLE_HAS_TEXT, "<table> must only contain tags");
    static_assert(error2 != gtk_html_error::TABLE_HAS_NOT_TR, "only <tr> tags are allowed inside <table>");
    static_assert(error2 != gtk_html_error::TR_HAS_NOT_TD, "only <td> tags are allowed inside <tr>");
    static_assert(error2 != gtk_html_error::OVERLAY_EMPTY_BODY, "<overlay> must have at least one child");
    static_assert(error2 != gtk_html_error::STYLE_NOT_STRING, "\"style\" attribute must be a string");
    static_assert(error2 != gtk_html_error::ALIGN_NOT_STRING, "\"align\" attribute must be a string");
    static_assert(error2 != gtk_html_error::INVALID_ALIGN, "\"align\" must be one of \"left\", \"center\", \"right\"");
    static_assert(error2 != gtk_html_error::VALIGN_NOT_STRING, "\"valign\" attribute must be a string");
    static_assert(error2 != gtk_html_error::INVALID_VALIGN, "\"valign\" must be one of \"top\", \"middle\", \"bottom\"");
    static_assert(error2 != gtk_html_error::OVERFLOW_NOT_STRING, "\"overflow\" attribute must be a string");
    static_assert(error2 != gtk_html_error::TEXTAREA_CONTENT_NOT_STRING, "\"textarea\" content must be a single text node");
    template<bool q, bool r>
    struct Getter
    {
        template<typename... Args>
        static inline auto get(Args... a)
        {
            return CallTreeImpl<tree::data.self->externalize(tree::data.self->call_root), tree>::get(a...);
        }
    };
    template<bool r>
    struct Getter<false, r>
    {
        template<typename... Args>
        static inline int get(Args... a)
        {
            return 0;
        }
    };
    template<typename... Args>
    static inline auto get(Args... a)
    {
        return Getter<error == parser_errors::NO_ERROR && error2 == gtk_html_error::NO_ERROR, false>::get(a...);
    }
};

}

}

#define GTKHTML(s, ...) (([&]\
{\
    constexpr char gtk_html_internal_s[] = s;\
    struct gtk_html_internal_StringWrapper\
    {\
        char data[sizeof(gtk_html_internal_s)] = s;\
    };\
    return gtk_html::internal::GtkHtmlImplementation<gtk_html_internal_StringWrapper>::get(__VA_ARGS__);\
})())

#endif
