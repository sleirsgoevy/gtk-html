#ifndef _PARSER_HPP
#define _PARSER_HPP
#include "call-tree.hpp"

namespace html_cpp
{

struct html_node;

struct html_attribute
{
    const char* name;
    html_node* value;
    html_attribute* next;
};

struct html_body
{
    html_node* cur;
    html_body* next;
};

struct html_tag
{
    const char* name;
    html_attribute* attrs;
    html_body* body;
};

enum class html_node_type
{
    END_OF_FILE,
    TEXT_NODE,
    START_TAG,
    END_TAG,
};

enum class parser_errors
{
    NO_ERROR,
    MEMORY_ERROR,
    TAG_NO_GT,
    MISMATCHED_END_TAG,
    TAG_NEVER_CLOSED,
    CLOSING_ATTRS,
    TAG_NO_QUOT,
    INVALID_SELFCLOSE,
    MISSING_ATTR_TAG,
    CLOSING_SELFCLOSE,
    UNTERMINATED_COMMENT,
};

struct html_node
{
    html_node_type type;
    /*union
    {*/
        html_tag tag;
        const char* text_node;
    //}
};

namespace internal
{

using namespace call_trees;

template<int memsz>
struct CompileTimeAllocator
{

template<typename T, bool q>
struct Allocator
{
    static constexpr T* alloc(CompileTimeAllocator* self, int n) =delete;
    static constexpr const T* internalize(const CompileTimeAllocator* self, int idx) =delete;
    static constexpr int externalize(const CompileTimeAllocator* self, const T* ptr) =delete;
};

template<typename T>
struct AllocatorInstance
{
    static constexpr int localsz = memsz / sizeof(T);
    T data[localsz]{};
    int idx = 0;
    constexpr T* alloc(int n)
    {
        if(idx + n > localsz)
            return nullptr;
        T* ans = data + idx;
        idx += n;
        int q = ans - data;
        return ans;
    }
};

#define ADD_TYPE(type)\
AllocatorInstance<type> allocator_of_ ## type;\
template<bool q>\
struct Allocator<type, q>\
{\
    static constexpr type* alloc(CompileTimeAllocator* self, int n)\
    {\
        return self->allocator_of_ ## type.alloc(n);\
    }\
    static constexpr const type* internalize(const CompileTimeAllocator* self, int idx)\
    {\
        return self->allocator_of_ ## type.data+idx;\
    }\
    static constexpr int externalize(const CompileTimeAllocator* self, const type* ptr)\
    {\
        const type* ptr2 = self->allocator_of_ ## type.data;\
        return ptr - ptr2;\
    }\
}

template<typename T>
constexpr T* alloc(int n = 1)
{
    return Allocator<T, false>::alloc(this, n);
}

template<typename T>
constexpr int externalize(const T* ptr) const
{
    if(ptr == nullptr)
        return -1;
    return Allocator<T, false>::externalize(this, ptr);
}

template<typename T>
constexpr const T* internalize(int ptr) const
{
    if(ptr == -1)
        return nullptr;
    return Allocator<T, false>::internalize(this, ptr);
}

ADD_TYPE(char);
typedef char* char_p;
ADD_TYPE(char_p);
ADD_TYPE(int);
typedef const char* const_char_p;
ADD_TYPE(const_char_p);
ADD_TYPE(html_node);
ADD_TYPE(html_attribute);
ADD_TYPE(html_body);
typedef html_body* html_body_p;
ADD_TYPE(html_body_p);
ADD_TYPE(call_tree);
typedef call_tree* call_tree_p;
ADD_TYPE(call_tree_p);

#undef ADD_TYPE

};

struct HTMLParserExtension
{
    constexpr html_node* post_process_pass(html_node* orig)
    {
        return orig;
    }
    constexpr html_body* get_root(html_body* parser_root, bool& ok)
    {
        ok = true;
        return parser_root;
    }
};

template<typename Alloc, typename Ext = HTMLParserExtension>
struct HTMLParser : Alloc, Ext
{

bool memory_error = false;

template<typename T>
constexpr T* alloc(int n = 1)
{
    T* ans = Alloc::template alloc<T>(n);
    if(!ans)
        memory_error = true;
    return ans;
}

parser_errors error_code = parser_errors::NO_ERROR;

template<typename T>
constexpr static int elemlen(const T* s)
{
    int n = 0;
    while(s[n])
        n++;
    return n;
}

constexpr static int strlen(const char* s)
{
    return elemlen(s);
}

template<typename T>
constexpr static void elemcpy(T* dst, const T* src, int n)
{
    for(int i = 0; i < n; i++)
        dst[i] = src[i];
}

constexpr static void strcpy(char* dst, const char* src)
{
    elemcpy(dst, src, strlen(src)+1);
}

//XXX: speed
constexpr char* str_concat(const char* a, const char* b)
{
    char* ans = alloc<char>(strlen(a)+strlen(b)+1);
    if(ans)
    {
        strcpy(ans, a);
        strcpy(ans+strlen(a), b);
    }
    return ans;
}

constexpr char* strdup(const char* s)
{
    char* out = alloc<char>(strlen(s)+1);
    if(out)
        strcpy(out, s);
    return out;
}

constexpr static bool isspace(char c)
{
    return c == '\t' || c == '\n' || c == ' ';
}

constexpr static char* trim(char* s)
{
    while(isspace(*s))
        s++;
    char* t = s + strlen(s);
    while(t != s && isspace(t[-1]))
        *--t = 0;
    return s;
}

constexpr static int strcmp(const char* a, const char* b)
{
    while(*a && *a == *b)
    {
        a++;
        b++;
    }
    return *a - *b;
}

constexpr char* parse_text(const char*& s)
{
    const char* start = s;
    while(*s && *s != '<')
        s++;
    char* ans = alloc<char>(s-start+1);
    elemcpy(ans, start, s-start);
    ans[s-start] = 0;
    return trim(ans);
}

constexpr char* parse_node(const char*& s, html_node_type& t, html_attribute*& attr_list, bool& is_selfclose)
{
    while(s[0] == '<' && s[1] == '!' && s[2] == '-' && s[3] == '-')
    {
        s += 4;
        while(s[0] && s[1] && s[2] && (s[0] != '-' || s[1] != '-' || s[2] != '>'))
            s++;
        if(!s[0] || !s[1] || !s[2])
        {
            error_code = parser_errors::UNTERMINATED_COMMENT;
            t = html_node_type::START_TAG;
            return nullptr;
        }
        s += 3;
    }
    if(*s == 0)
    {
        t = html_node_type::END_OF_FILE;
        return nullptr;
    }
    else if(*s != '<')
    {
        t = html_node_type::TEXT_NODE;
        return parse_text(s);
    }
    else
    {
        s++;
        if(*s == '/')
        {
            s++;
            t = html_node_type::END_TAG;
        }
        else
            t = html_node_type::START_TAG;
        const char* start_name = s;
        while(*s && *s != '/' && *s != '>' && !isspace(*s))
            s++;
        if(!*s)
        {
            error_code = parser_errors::TAG_NO_GT;
            return nullptr;
        }
        char* tag_name = alloc<char>(s+1-start_name);
        if(!tag_name)
            return nullptr;
        elemcpy(tag_name, start_name, s-start_name);
        tag_name[s-start_name] = 0;
        if(t == html_node_type::END_TAG)
        {
            if(isspace(*s))
            {
                error_code = parser_errors::CLOSING_ATTRS;
                return nullptr;
            }
            if(*s == '/')
            {
                error_code = parser_errors::CLOSING_SELFCLOSE;
                return nullptr;
            }
            s++;
            return tag_name;
        }
        attr_list = nullptr;
        for(;;)
        {
            while(isspace(*s))
                s++;
            if(*s == '>' || *s == '/')
                break;
            const char* start_attr = s;
            while(*s && !isspace(*s) && *s != '=' && (*s != '>' && *s != '/'))
                s++;
            if(!*s)
            {
                error_code = parser_errors::TAG_NO_GT;
                return nullptr;
            }
            char* attr_name = alloc<char>(s+1-start_attr);
            elemcpy(attr_name, start_attr, s-start_attr);
            attr_name[s-start_attr] = 0;
            html_node* attr_value = alloc<html_node>();
            if(!attr_value)
                return nullptr;
            attr_value->type = html_node_type::TEXT_NODE;
            if(*s++ != '=')
            {
                s--;
                attr_value->text_node = attr_name;
            }
            else if(*s != '<')
            {
                char c = *s;
                const char* start_attr_value = s;
                if(c == '"')
                {
                    s++;
                    start_attr_value = s;
                    while(*s && *s != '"')
                        s++;
                    if(!*s)
                    {
                        error_code = parser_errors::TAG_NO_QUOT;
                        return nullptr;
                    }
                }
                else
                {
                    while(*s && *s != '/' && *s != '>' && !isspace(*s))
                        s++;
                }
                char* text = alloc<char>(s-start_attr_value+1);
                attr_value->text_node = text;
                elemcpy(text, start_attr_value, s-start_attr_value);
                text[s-start_attr_value] = 0;
                if(c == '"')
                    s++;
            }
            else
            {
                html_node_type nt = html_node_type::END_TAG;
                bool ok = false;
                html_node* value = parse_subtree(s, nt, nullptr, ok);
                if(!ok)
                    return nullptr;
                if(!value)
                {
                    error_code = parser_errors::MISSING_ATTR_TAG;
                    return nullptr;
                }
                *attr_value = *value;
            }
            html_attribute* new_attr = alloc<html_attribute>();
            new_attr->name = attr_name;
            new_attr->value = attr_value;
            new_attr->next = attr_list;
            attr_list = new_attr;
        }
        is_selfclose = false;
        if(*s == '/')
        {
            is_selfclose = true;
            s++;
            if(*s != '>')
            {
                error_code = parser_errors::INVALID_SELFCLOSE;
                return nullptr;
            }
        }
        s++;
        t = html_node_type::START_TAG;
        return tag_name;
    }
}

constexpr html_node* parse_subtree(const char*& s, html_node_type& node_type, const char* end_tag, bool& ok)
{
    ok = false;
    const char* start_tag = nullptr;
    node_type = html_node_type::TEXT_NODE;
    html_attribute* attr_list = nullptr;
    bool is_selfclose = false;
    do
        start_tag = parse_node(s, node_type, attr_list, is_selfclose);
    while(node_type == html_node_type::TEXT_NODE && (!start_tag || !*start_tag));
    if(!start_tag)
    {
        if(node_type == html_node_type::END_OF_FILE)
            ok = true;
        return nullptr;
    }
    if(node_type == html_node_type::TEXT_NODE)
    {
        html_node* ans = alloc<html_node>();
        if(!ans)
            return nullptr;
        ans->type = html_node_type::TEXT_NODE;
        ans->text_node = start_tag;
        ok = true;
        return Ext::post_process_pass(ans);
    }
    if(node_type == html_node_type::END_TAG)
    {
        if(!end_tag || strcmp(start_tag, end_tag))
        {
            error_code = parser_errors::MISMATCHED_END_TAG;
            return nullptr;
        }
        ok = true;
        return nullptr;
    }
    else if(node_type == html_node_type::START_TAG)
    {
        if(is_selfclose)
        {
            html_node* ans = alloc<html_node>();
            if(!ans)
                return nullptr;
            ans->type = html_node_type::START_TAG;
            ans->tag.name = start_tag;
            ans->tag.attrs = attr_list;
            ans->tag.body = nullptr;
            ans = Ext::post_process_pass(ans);
            if(ans)
                ok = true;
            return ans;
        }
        html_node* ans = alloc<html_node>();
        if(!ans)
            return nullptr;
        ans->type = html_node_type::START_TAG;
        ans->tag.name = start_tag;
        ans->tag.attrs = attr_list;
        ans->tag.body = parse_subforest(s, start_tag, ok);
        if(!ok)
            return nullptr;
        ok = false;
        ans = Ext::post_process_pass(ans);
        if(ans)
            ok = true;
        return ans;
    }
    else
        return nullptr;
}

constexpr html_body* parse_subforest(const char*& s, const char* end_tag, bool& ok)
{
    html_body* head = nullptr;
    html_body** last = &head;
    for(;;)
    {
        ok = false;
        html_node_type node_type = html_node_type::END_OF_FILE;
        html_node* q = parse_subtree(s, node_type, end_tag, ok);
        if(!ok)
            return nullptr;
        if(!q)
        {
            if(end_tag && node_type == html_node_type::END_OF_FILE)
            {
                error_code = parser_errors::TAG_NEVER_CLOSED;
                return nullptr;
            }
            return head;
        }
        html_body* ln = alloc<html_body>();
        if(!ln)
        {
            ok = false;
            return nullptr;
        }
        *last = ln;
        last = &ln->next;
        ln->cur = q;
        ln->next = nullptr;
    }
}

constexpr html_body* parse(const char* html, bool& ok)
{
    html_body* data = parse_subforest(html, nullptr, ok);
    if(!ok)
        return nullptr;
    return Ext::get_root(data, ok);
}

};

}

using internal::CompileTimeAllocator;
using internal::HTMLParser;
using internal::HTMLParserExtension;

}

#endif
