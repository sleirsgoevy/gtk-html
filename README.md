# gtk-html

gtk-html is a C++ header library that implements "almost a subset" of HTML in GTK. This lets you write GTK interfaces very similar to how you write web forms.

## Installation

Just copy the header files to your project's include path. You'll need GTK4 as a dependency.

## "Hello, world!"

```
#include "gtk-html/gtk-html.hpp"

static void run(GtkApplication* app) //userdata omitted
{
    GTKHTML("<window noresize show>Hello, world!</window>", app); //<window> accepts app as an argument
}
GTKHTML_GLOBAL_CALLBACK(run); //callbacks declared explicitly

int main()
{
    //one simple macro for multiple uses
    //html-style event handler declaration (this is not js, so its just a function name)
    //run: who said HTML is not a programming language?
    GTKHTML("<application id=com.example.helloworld onactivate=run run/>");
}
```

See more examples in the `examples/` directory.

## Features

- Supports a subset of GTK widgets, can be easily extended.
- GTK widgets that exist in HTML are mentioned by their HTML names.
 - `<input> = GtkEntry`
 - Likewise, attributes and event names are modeled after HTML.
 - Concepts that do not exist in HTML are written using their GTK names.
- Uses GTK's C API. No gtkmm here.
- Compile-time.

## Missing features

- Easy to use extension API (e.g. add new tags). (Planned)
- Runtime API. After the HTML is instantiated, you can use normal GTK functions.
- Runtime HTML parser. The first argument to GTKHTML must be a string literal. (Planned)
- Using arbitrary GTK widgets by their names. Currently each widget must be hardcoded in the code. (Planned)
- Support for languages other than C++. (Rust and C planned)
